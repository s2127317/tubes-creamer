/*
  ==============================================================================

    Chorus.h
    Created: 5 Mar 2021 11:43:13am
    Author:  Tom Mudd

  ==============================================================================
*/

#pragma once

#include "DelayLine.h"
#include "Oscillators.h"


class Chorus
{
    
public:
    
    void setSampleRate(float sampleRate)
    {
        lfoA.setSampleRate(sampleRate);
        lfoB.setSampleRate(sampleRate);
        
        lfoA.setFrequency(rate);
        lfoB.setFrequency(rate * 1.714265673657f);
        
        delayA.setSize(sampleRate * 0.5);
        delayB.setSize(sampleRate * 0.5);
        
        delayA.setFeedback(feedback);
        delayB.setFeedback(feedback);
        
        delayA.setDelayTimeInSamples(sampleRate * 0.25);
        delayB.setDelayTimeInSamples(sampleRate * 0.25);
    }
    
    
    float process(float inSample)
    {
        float lfoAOut = lfoA.process();     // between -1 and +1
        float lfoBOut = lfoB.process();
        
        delayA.setDelayTimeInSamples(4000.0f + lfoAOut*(3000.0f * depth));
        delayB.setDelayTimeInSamples(3000.0f + lfoBOut*(2000.0f * depth));
        
        float delayAOut = delayA.process(inSample);
        float delayBOut = delayB.process(inSample);
        
        return delayAOut + delayBOut;
    }
    
    void setDepth(float _depth)
    {
        depth = _depth;
    }
    
    void setRate(float _rate)
    {
        rate = _rate;
        lfoA.setFrequency(rate);
        lfoB.setFrequency(rate * 1.714265673657f);
    }
    
    
    
private:
    DelayLine delayA;
    DelayLine delayB;
    
    SinOsc lfoA;
    SinOsc lfoB;
    
    float rate = 0.25f;             // rate of oscillation of delay times
    
    float depth = 0.25f;
    
    float feedback = 0.25f;
};
