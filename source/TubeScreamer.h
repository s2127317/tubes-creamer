/*
  ==============================================================================

    TubeScreamer.h
    Created: 4 May 2021 10:32:44pm
    Author:  刘子源

  ==============================================================================
*/

#pragma once
#include "MatrixMultiply.h"

class TubeScreamer
{
public:
    
    struct CircuitParams
    {
        // resistors
        float r1;
        float r2;
        float r3;
        float r4;
        float r_dist;
        
        // capacitors
        float c1;
        float c2;
        float c3;
        
        // diode saturation current
        float Is;
        
        // diode thermal voltage
        float Vt;
        
        // diode ideality factor
        float Ni;
        
    };
    
    void init(float sampleRate, int number)
    {
        SR = sampleRate;
        k = 1/SR;
        
        circuitParams1.r1 = 5.0e2f;
        circuitParams1.r2 = 51.0e3f;
        circuitParams1.r3 = 4.7e4f;
        circuitParams1.r4 = 1.0e6f;
        circuitParams1.r_dist = 5.0e3f;
        circuitParams1.c1 = 1e-6f;
        circuitParams1.c2 = 5.1e-11f;
        circuitParams1.c3 = 4.7e-2f;
        circuitParams1.Is = 2.52e-9f;
        circuitParams1.Vt = 2.583e-2f;
        circuitParams1.Ni = 1.752f;
        
        circuitParams2.r1 = 10.0e3f;
        circuitParams2.r2 = 51.0e3f;
        circuitParams2.r3 = 4.7e3f;
        circuitParams2.r4 = 1.0e6f;
        circuitParams2.r_dist = 5.0e3f;
        circuitParams2.c1 = 1e-6f;
        circuitParams2.c2 = 51e-12f;
        circuitParams2.c3 = 47e-9f;
        circuitParams2.Is = 2.52e-9f;
        circuitParams2.Vt = 25.83e-3f;
        circuitParams2.Ni = 1.752f;
        
        if (number == 1)
        {
            circuitParams = circuitParams1;
        }
        else if (number == 2)
        {
            circuitParams = circuitParams2;
        }
        
        A[0][0] = -1/(circuitParams.r1*circuitParams.c1);
        A[0][1] = 0.0f;
        A[0][2] = 0.0f;
        A[1][0] = 1/(circuitParams.r3*circuitParams.c2);
        A[1][1] = 1/((circuitParams.r2+circuitParams.r_dist)*circuitParams.c2);
        A[1][2] = 1/(circuitParams.r3*circuitParams.c2);
        A[2][0] = 1/(circuitParams.r3*circuitParams.c3);
        A[2][1] = 0.0f;
        A[2][2] = 1/(circuitParams.r3*circuitParams.c3);
        
        B[0][0] = 1/(circuitParams.r1*circuitParams.c1);
        B[1][0] = 1/(circuitParams.r3*circuitParams.c2);
        B[2][0] = 1/(circuitParams.r3*circuitParams.c3);
        
        C[0][0] = 0.0f;
        C[1][0] = -1/circuitParams.c2;
        C[2][0] = 0.0f;
        
        D[0] = 0.0f;
        D[1] = 1.0f;
        D[2] = 0.0f;
    
        L[0] = -1.0f;
        L[1] = 1.0f;
        L[2] = 0.0f;
        
        for (int i=0; i<3; i++)
        {
            for (int j=0; j<3; j++)
            {
                if (i != j){
                    I[i][j] = 0.0f;
                }
                else if(i == j){
                    I[i][j] = 1.0f;
                }
            }
        }
        
        
        
        for (int i=0; i<3; i++)
        {
            for (int j=0; j<3; j++)
            {
                Ha1[i][j] = (2/k)*I[i][j] - A[i][j];
                Hb[i][j] = (2/k)*I[i][j] + A[i][j];
            }
        }
        
        matrixMultiply.inverse33(Ha1,Ha);
        
        
        matrixMultiply.multiply33by31(Ha,C,K1);
        K = matrixMultiply.multiply13by31(D, K1) + F;
        
        G = 2*circuitParams.Is/(circuitParams.Ni*circuitParams.Vt);
        
        x1[0][0] = 0.0f;
        x1[1][0] = 0.0f;
        x1[2][0] = 0.0f;
        
        v = 0.0f;
        
        
    }
    
    float process(float sample)
    {
        matrixMultiply.multiply33by31(Hb,x1,Hb1);
        matrixMultiply.multiply13by33(D,Ha,Ha2);
        p2 = matrixMultiply.multiply13by31(Ha2,B);
        
        
        
        for (int i=0; i<3; i++)
        {
            t[i][0] = Hb1[i][0] + u1*B[i][0] + i1*C[i][0];
        }
        
        p1 = matrixMultiply.multiply13by31(Ha2,t);
        
        u0 = sample;
        
        p = p1 + (p2+E)*u0;
        
        delta_v = 1.0f;
        iter = 0;
        
        while ((abs(delta_v) > tol) && (iter < maxnum))
        {
            g = 2*circuitParams.Is*K*sinh(v/(circuitParams.Vt*circuitParams.Ni)) - v + p;
            J =((2*circuitParams.Is*K)/(circuitParams.Ni*circuitParams.Vt))*cosh(v/(circuitParams.Ni*circuitParams.Vt)) - 1;
            
            delta_v = g/J;
            
            v = v-delta_v;
            
            iter = iter + 1;
        }
        
        
        i0 = (v-p)/K;
        
        
        for (int i=0; i<3; i++)
        {
            Hb2[i][0] = Hb1[i][0] + (u0+u1)*B[i][0] + (i0+i1)*C[i][0];
        }
        
        matrixMultiply.multiply33by31(Ha,Hb2,x0);
        
        output = matrixMultiply.multiply13by31(L,x0) + M*u0 + N*i0;
        
        u1 = u0;
        i1 = i0;
        matrixMultiply.copymatrix(x1, x0);
        
        return 0.5;
    }
    
private:
    
    // define 2 set of circuit parameters
    CircuitParams circuitParams1;
    CircuitParams circuitParams2;
    CircuitParams circuitParams;
    
    // for matrix calculation
    MatrixMultiply<float> matrixMultiply;
    
    float SR;        // sampleRate
    
    // initialisations
    float A[3][3];
    float B[3][1];
    float C[3][1];
    float D[3];
    float L[3];
    
    float E = 0.0f;
    float F = 0.0f;
    float M = 1.0f;
    float N = 0.0f;
    
    float Ha[3][3];
    float Ha1[3][3];  // for calculation of Ha inverse
    float Ha2[3];  // for calculation of D*Ha
    
    float I[3][3];
    
    float Hb[3][3];
    float Hb1[3][1];   // for calculation of Hb*x1
    float Hb2[3][1];   // for calculation of Hb*x1 + B*(u0+u1) + C*(i0 + i1)
    
    float t[3][1];     // for calculation of Hb*x1 + B*u1 + C*i1
    
    float p;
    float p1;          // for calcualtion of D*Ha*(Hb*x1 + B*u1 + C*i1)
    float p2;          // for calculation of D*Ha*B
   
    float K1[3][1];    // for calculation of Ha*C
    float K;
    float k;           // time step
    
    float G;
    
    float x1[3][1];
    float i1 = 0.0f;
    float u1 = 0.0f;

    float tol = 1.0e-10f;
    int maxnum = 100;
    int iter;
    
    float g;
    float J;
    float delta_v;
    float v;
    
    
    float i0;
    float x0[3][1];
    float u0;
    
    
    float output;       // output for input sample
    ;
    
};
