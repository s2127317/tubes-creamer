/*
  ==============================================================================

    DiodeClipper.h
    Created: 5 May 2021 6:03:24am
    Author:  刘子源

  ==============================================================================
*/

#pragma once

class DiodeClipper
{
public:
    
    void init(float sampleRate)
    {
        k = 1/sampleRate;
        
        r = 1e3;                                 // resistance of R [Ohms]
        c = 33e-9;                               // capacitance of C [F]
        Is = 2.52e-9;                            // diode saturation current (A)
        Vt = 25.83e-3;                           // diode thermal voltage (V)
        Ni = 1.752;                              // diode ideality factor
        
        
        Ha = 1/(2/k - A);
        Hb = 2/k + A;
        K = D * Ha * C + F;
        G = 2*Is/(Ni*Vt);
        
        x1 = 0.0f;
        i1 = 0.0f;
        u1 - 0.0f;
        
        
        
    }
    
    float process(float sample)
    {
        u0 = sample;
        
        p = D*Ha*(Hb*x1 + B*u1 + C*i1) + (D*Ha*B + E)*u0;
        
        v = p/(1-K*G);
        
        i0 = (v-p)/K;
        
        x0 = Ha * (Hb*x1 + B*(u0+u1) + C*(i0 + i1));
        
        float output = L*x0 + M*u0 + N*i0;
        
        i1 = i0;
        u1 = u0;
        x1 = x0;
        
        return output;
    }
    
    
private:
    float r;
    float c;
    float Is;
    float Vt;
    float Ni;
    
    float A;
    float B;
    float C;
    float D = 1.0f;
    float E = 0.0f;
    float F = 0.0f;
    float L = 1.0f;
    float M = 0.0f;
    float N = 0.0f;
    
    float Ha;
    float Hb;
    float K;
    float G;
    
    float x1;
    float i1;
    float u1;
    
    float x0;
    float i0;
    float u0;
    
    float k;
    float p;
    float v;
    
    
};
