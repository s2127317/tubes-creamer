/*/Users/ziyuan/Downloads/TubeScreamer-plugin-master/TubeScreamer/Source/Matrices.h
  ==============================================================================

    MatrixMultiply.h
    Created: 4 May 2021 9:04:41pm
    Author:  刘子源

  ==============================================================================
*/

#pragma once
#ifndef MatrixMultiply_h
#define MatrixMultiply_h
using namespace std;
template <class temp>

class MatrixMultiply
{
public:
    
    // copy b to a, means a = b
    void copymatrix(temp a[3][1], temp b[3][1])
    {
        for (int i=0; i<3; i++)
        {
            a[i][0] = b[i][0];
        }
    }
    
    // calculate the multiplication of a 3*3 matrix and a 3*1 matrix
    void multiply33by31(temp a[3][3], temp b[3][1], temp c[3][1])
    {
        for (int i=0; i<3; i++)
        {
            c[i][0] = 0.0f;
            for (int j=0; j<3; j++)
            {
                c[i][0] += a[i][j] * b[j][0];
            }
        }
    }
    
    // calculate the multiplication of a 1*3 matrix and a 3*3 matrix
    void multiply13by33(temp a[3], temp b[3][3], temp c[3])
    {
        for (int i=0; i<3; i++)
        {
            c[i] = 0.0f;
            for (int j=0; j<3; j++)
            {
                c[j] += a[j] * b[j][i];
            }
        }
    }
    
    // calculate the multiplication of a 3*3 matrix and a 3*3 matrix
    
    void multiply33by33(temp a[3][3], temp b[3][3], temp c[3][3])
    {
        for (int i=0; i<3; i++)
        {
            for (int j=0; j<3; j++)
            {
                c[i][j] = 0.0f;
                
                for (int m=0; m<3; m++)
                {
                    c[i][j] += a[i][m] * b[m][j];
                }
            }
        }
    }
    
    
    // calculate the multiplication of a 1*3 matrix and a 3*1 matrix
    temp multiply13by31(temp a[3], temp b[3][1])
    {
        temp c = 0.0f;
        
        for (int i=0; i<3; i++)
        {
            c += a[i] * b[i][0];
        }
        
        return c;
    }
    
    //calculate the inverse matrix of a 3*3 matrix
    void inverse33(temp a[3][3], temp c[3][3])
    {
        // initialiase a 3*3 matrix for the inverse matrix
        temp b[3][3];
        
        
        //calculate the minor matrix
        b[0][0] = a[1][1] * a[2][2] - a[1][2] * a[2][1];
        b[0][1] = a[1][0] * a[2][2] - a[1][2] * a[2][0];
        b[0][2] = a[1][0] * a[2][1] - a[1][1] * a[2][0];
        b[1][0] = a[0][1] * a[2][2] - a[0][2] * a[2][1];
        b[1][1] = a[0][0] * a[2][2] - a[0][2] * a[2][0];
        b[1][2] = a[0][0] * a[2][1] - a[0][1] * a[2][0];
        b[2][0] = a[0][1] * a[1][2] - a[0][2] * a[1][1];
        b[2][1] = a[0][0] * a[1][2] - a[0][2] * a[1][0];
        b[2][2] = a[0][0] * a[1][1] - a[0][1] * a[1][0];
        
        temp det_a = a[0][0] * b[0][0] - a[0][1] * b[0][1] + a[0][2] * b[0][2];
        
        // transpose and calculate the inverse matrix
        for (int i=0; i<3; i++)
        {
            for (int j=0; j<3; j++)
            {
                a[i][j] = powf(-1,3*j+j) * b[j][i];
                c[i][j] = a[i][j]/det_a;
            }
        }
        
        
        
    }
    
    
    
};

#endif
