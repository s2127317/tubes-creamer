/*
  ==============================================================================

    MySynthesiser.h
    Created: 7 Mar 2020 4:27:57pm
    Author:  Tom Mudd

  ==============================================================================
*/

#pragma once
#include "Oscillators.h"
#include "TubeScreamer.h"
#include "OscSynth.h"
#include "DiodeClipper.h"
#include "Wind.h"
#include "FootStep.h"

// ===========================
// ===========================
// SOUND
class MySynthSound : public juce::SynthesiserSound
{
public:
    bool appliesToNote      (int) override      { return true; }
    //--------------------------------------------------------------------------
    bool appliesToChannel   (int) override      { return true; }
};




// =================================
// =================================
// Synthesiser Voice - your synth code goes in here

/*!
 @class MySynthVoice
 @abstract struct defining the DSP associated with a specific voice.
 @discussion multiple MySynthVoice objects will be created by the Synthesiser so that it can be played polyphicially
 
 @namespace none
 @updated 2019-06-18
 */
class MySynthVoice : public juce::SynthesiserVoice
{
public:
    MySynthVoice() {}
    
    void init(float sampleRate)
    {
        osc.setSampleRate(sampleRate);
        detuneOsc.setSampleRate(sampleRate);
        env.setSampleRate(sampleRate);
        tubescreamer.init(sampleRate, 1);
        oscSynth.setSampleRate(sampleRate);
        
        diodeClipper.init(sampleRate);
        
        wind.setSampleRate(sampleRate);
        
        
        footStep.setSampleRate(sampleRate);
       
        
        juce::ADSR::Parameters envParams;
        envParams.attack = 0.5f;
        envParams.decay = 0.25f;
        envParams.sustain = 0.5f;
        envParams.release = 1.0f;
        
        env.setParameters(envParams);
        
    }
    
    void setParameterPointers(std::atomic<float>* detuneIn)
    {
        detuneAmount = detuneIn;
    }
    //--------------------------------------------------------------------------
    /**
     What should be done when a note starts

     @param midiNoteNumber
     @param velocity
     @param SynthesiserSound unused variable
     @param / unused variable
     */
    void startNote (int midiNoteNumber, float velocity, juce::SynthesiserSound*, int /*currentPitchWheelPosition*/) override
    {
        playing = true;
        ending = false;
       freq = juce::MidiMessage::getMidiNoteInHertz(midiNoteNumber);
        osc.setFrequency(freq);
        
        oscSynth.setLfoFrequency(freq);
        
        wind.setLfoFrequency(freq);
        
        footStep.setFrequency(freq);
        
        env.reset();
        env.noteOn();
       
        
    }
    //--------------------------------------------------------------------------
    /// Called when a MIDI noteOff message is received
    /**
     What should be done when a note stops

     @param / unused variable
     @param allowTailOff bool to decie if the should be any volume decay
     */
    void stopNote(float /*velocity*/, bool allowTailOff) override
    {
        env.noteOff();
        ending = true;
        
    }
    
    //--------------------------------------------------------------------------
    /**
     The Main DSP Block: Put your DSP code in here
     
     If the sound that the voice is playing finishes during the course of this rendered block, it must call clearCurrentNote(), to tell the synthesiser that it has finished

     @param outputBuffer pointer to output
     @param startSample position of first sample in buffer
     @param numSamples number of smaples in output buffer
     */
    void renderNextBlock(juce::AudioSampleBuffer& outputBuffer, int startSample, int numSamples) override
    {
        if (playing) // check to see if this voice should be playing
        {
            
//            float* left = outputBuffer.getWritePointer(0);
//            float* right = outputBuffer.getWritePointer(1);
//
//            left = phaseVocoder.process(left);
//            right = phaseVocoder.process(right);
            
            
            
            
            // iterate through the necessary number of samples (from startSample up to startSample + numSamples)
            for (int sampleIndex = startSample;   sampleIndex < (startSample+numSamples);   sampleIndex++)
            {
                float envVal = env.getNextSample();
                
                detuneOsc.setFrequency(freq - *detuneAmount);
                // your sample-by-sample DSP code here!
                // An example white noise generater as a placeholder - replace with your own code
                float currentSample = (osc.process() + detuneOsc.process() ) * 0.5f * envVal;
                
////                currentSample = diodeClipper.process(currentSample);
                currentSample = (currentSample*gain_sample + oscSynth.process()*gain_oscSynth + wind.process()*gain_wind + footStep.process()*gain_footstep);
                
//                currentSample = tubescreamer.process(currentSample);
//
                // for each channel, write the currentSample float to the output
                for (int chan = 0; chan<outputBuffer.getNumChannels(); chan++)
                {
                    // The output sample is scaled by 0.2 so that it is not too loud by default
                    outputBuffer.addSample (chan, sampleIndex, currentSample * 0.2);
                }
                
                if (ending)
                {
                    if (envVal < 0.0001f)
                    {
                        clearCurrentNote();
                        playing = false;
                    }
                }
            }
        }
    }
    //--------------------------------------------------------------------------
    void pitchWheelMoved(int) override {}
    //--------------------------------------------------------------------------
    void controllerMoved(int, int) override {}
    //--------------------------------------------------------------------------
    /**
     Can this voice play a sound. I wouldn't worry about this for the time being

     @param sound a juce::SynthesiserSound* base class pointer
     @return sound cast as a pointer to an instance of MySynthSound
     */
    bool canPlaySound (juce::SynthesiserSound* sound) override
    {
        return dynamic_cast<MySynthSound*> (sound) != nullptr;
    }
    //--------------------------------------------------------------------------
private:
    //--------------------------------------------------------------------------
    // Set up any necessary variables here
    /// Should the voice be playing?
    bool playing = false;
    bool ending = false;

    /// a random object for use in our test noise function
    juce::Random random;
    
    float freq;
    
    TriOsc osc, detuneOsc;
    
    std::atomic<float>* detuneAmount;
    
    juce::ADSR env;       // our ADSR envelope
    
    TubeScreamer tubescreamer;
    
    OscSynth oscSynth;
    
    DiodeClipper diodeClipper;
    
    Wind wind;
    
    FootStep footStep;
    
    float gain_sample;
    float gain_wind;
    float gain_oscSynth;
    float gain_footstep;
    
    
};
