/*
  ==============================================================================

    FootStep.h
    Created: 9 Mar 2021 1:00:27pm
    Author: Ziyuan Liu
    create a class for footstep processing. create a square oscillator, a triangle oscillator, and 2 sin oscillators. Use lfo and square to set frequency for tri; and then use tri to modulate the squareOsc.

  ==============================================================================
*/

#pragma once

#include "Oscillators.h"
#include <JuceHeader.h>

class FootStep
{
public:
    
//    setter for samplerate
    void setSampleRate(float SampleRate)
    {
        FS_squareOsc.setSampleRate(SampleRate);
        FS_lfoOsc.setSampleRate(SampleRate);
        FS_triOsc.setSampleRate(SampleRate);
    }
    
//    setter for frequency
    void setFrequency(float frequency)
    {
        FS_lfoOsc.setFrequency(frequency);
        FS_squareOsc.setFrequency(frequency);
        
    }
    
    float process()
    {
//      frequency definition
        float frequency = (FS_lfoOsc.process() + FS_squareOsc.process()) * 0.5f;

//        set frequency for FS_triOsc
        FS_triOsc.setFrequency(frequency);
        
//        final processing
        float Out_FS = FS_squareOsc.process() * FS_triOsc.process();
       
//        return value
        return Out_FS;
    }
private:
    
//    Basic Oscillators
    SquareOsc FS_squareOsc;
    SinOsc FS_lfoOsc;
    
//    modulation
    TriOsc FS_triOsc;
    
};
