/*
  ==============================================================================

    Synth.h
    Created: 2 Mar 2021 9:43:28pm
    Author:  Ziyuan Liu
    This is a class for Oscillator synthesis. I use two sin Oscillators for basic and then use another two sin Oscillatos for modulation and control.

  ==============================================================================
*/

#pragma
#include "Oscillators.h"
#include <JuceHeader.h>


class OscSynth
{
public:

//    SampleRate setter
    void setSampleRate(float SampleRate)
    {
        OS_sinOsc_1.setSampleRate(SampleRate);
        OS_sinOsc_2.setSampleRate(SampleRate);
        OS_lfo.setSampleRate(SampleRate);
        OS_volLfo.setSampleRate(SampleRate);
    }

//    OS_lfoFrequency setter
    void setLfoFrequency(float frequency)
    {
        OS_lfo.setFrequency(frequency);
    }

    
//    Note setter
    void setNote(int midiNote)
    {
        float frequency = juce::MidiMessage::getMidiNoteInHertz(midiNote);
//        beats effect
        OS_sinOsc_1.setFrequency(frequency);
        OS_sinOsc_2.setFrequency(frequency + 1.0f);
    }

//   processing method
    float process()
    {   
//        definition of oscOut for later processing and cotrol the value
        float oscOut = (OS_sinOsc_1.process() + OS_sinOsc_2.process()) * 0.5f;
        
//        set frequencyuency for volLfo and control the value
        OS_volLfo.setFrequency((OS_lfo.process() * 0.5f + 0.5f) * 0.5f + 0.05f);
        
//        definition for OS_lfo
        float OS_lfoVal = OS_volLfo.process() * 0.5f + 0.5f;
        
//        modulation
        float out_OS = oscOut * 0.5f * OS_lfoVal;
        
//        return value
        return out_OS;
    }

private:
 
//    basic oscillators
    SinOsc OS_sinOsc_1;
    SinOsc OS_sinOsc_2;
    
//    frequency set for volume control
    SinOsc OS_lfo;
    
//    volume controller
    SinOsc OS_volLfo; 
};
