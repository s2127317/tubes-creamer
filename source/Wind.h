/*
  ==============================================================================

    Wind.h
    Created: 4 Mar 2021 7:39:58pm
    Author:  Ziyuan Liu
    This class is set for wind sound. I set five sinOsc for basics and another two sinOsc for modulation and control.

  ==============================================================================
*/

#pragma once
#include "Oscillators.h"
#include <JuceHeader.h>

class Wind
{
public:
    
    
//    SampleRate setter
    void setSampleRate(float SampleRate)
    {
        
        Wind_sinOsc_1.setSampleRate(SampleRate);
        Wind_sinOsc_2.setSampleRate(SampleRate);
        Wind_sinOsc_3.setSampleRate(SampleRate);
        Wind_sinOsc_4.setSampleRate(SampleRate);
        Wind_sinOsc_5.setSampleRate(SampleRate);
        
        Wind_lfoOsc_1.setSampleRate(SampleRate);
        Wind_volLfo_1.setSampleRate(SampleRate);
    }
    
//    lfoFrequency setter
    void setLfoFrequency(float frequency)
    {
        Wind_lfoOsc_1.setFrequency(frequency);
    }
    
    
//    Note setter
    void setNote(int midiNote)
    {   // ===== sinOsc + Beating ===== //
        float frequency = juce::MidiMessage::getMidiNoteInHertz(midiNote);

        Wind_sinOsc_1.setFrequency(frequency);
        Wind_sinOsc_2.setFrequency(frequency + 1.0f);
        Wind_sinOsc_3.setFrequency(frequency + 2.0f);
        Wind_sinOsc_4.setFrequency(frequency + 3.0f);
        Wind_sinOsc_5.setFrequency(frequency + 4.0f);
        
    }
    
//    processing method
    float process()
    {
        
        
//        sum all together
        float oscOut = (Wind_sinOsc_1.process() + Wind_sinOsc_2.process() + Wind_sinOsc_3.process() + Wind_sinOsc_4.process() + Wind_sinOsc_5.process()) * 0.2f;
        
//        set frequencyuency for volLfo
        Wind_volLfo_1.setFrequency(Wind_lfoOsc_1.process() * 0.5f + 0.45f);
        
//        definition for lfo
        float lfoVal = Wind_volLfo_1.process() * 0.5f + 0.5f;
        
//        definition for output
        float outVal = oscOut * 0.5f * lfoVal;
        
//        return value
        return outVal;
        
       
    }

private:

//    Basic Oscillators
    SinOsc Wind_sinOsc_1;
    SinOsc Wind_sinOsc_2;
    SinOsc Wind_sinOsc_3;
    SinOsc Wind_sinOsc_4;
    SinOsc Wind_sinOsc_5;
    
//    modulation oscillator
    SinOsc Wind_lfoOsc_1;
    
//    volume control
    SinOsc Wind_volLfo_1;
    
};
