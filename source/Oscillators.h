//Oscillator.h
//PointorTest
//
//Created by tmudd on 06/02/2020
//Copyright © 2020 tmudd. All rights reserved


#ifndef Oscillator_h
#define Oscillator_h

#include <cmath>


// =============================================================================
// Parent Phasor Class START
   // parent oscillator class does the key things required for most oscillator
   // -- handles phase
   // -- handles setters and getters for frequency and sampleRate
// =============================================================================
class Phasor
{
public:

	float process()
	{
		phase += phaseDelta;
		
		if (phase > 1.0)
			phase -= phase;

		return output(phase);
	}

	virtual float output(float p)
	{
		return p;
	}

	void setSampleRate(float SR)
	{
		sampleRate = SR;

	}

	void setFrequency(float freq)
	{
		frequency = freq;

		phaseDelta = frequency / sampleRate;
	}

private:
	float frequency;
	float sampleRate;
	float phase = 0.0f;
	float phaseDelta;

};
// =============================================================================
// Parent Phasor Class END
// =============================================================================




// =============================================================================
// Child Classes START
   // Child class contain 4 basic oscillators.(saw / triangle / sine / square)
// =============================================================================

// 1 - Saw Wave oscillator
class SawOsc :public Phasor
{
	float output(float p) override
	{
		return p - 0.5f * 2.0f;
	}
};


// 2 - Triangle Wave oscillator
class TriOsc :public Phasor
{
	float output(float p) override
	{
		return fabsf(p - 0.5f) - 0.5f;
	}
};


// 3 - Sine Wave oscillator
class SinOsc :public Phasor
{
	float output(float p) override
	{
		return std::sin(p * 2.0 * 3.14159265358979323846);
	}
};


// 4 - Square Wave oscillator (level:-0.5f - 0.5f)
class SquareOsc :public Phasor
{
public:
	float output(float p) override
	{
		float outVal = 0.5f;
		if (p > pulseWidth)
			outVal = -0.5f;
		return outVal;		
	}
	void setPulseWidth(float pw)
	{
		pulseWidth = pw;
	}
    
    

private:
	float pulseWidth = 0.5f;
};



//  - Sine Wave oscillator
class Sin20Osc :public Phasor
{
	float output(float p) override
	{
		float sinVal = std::sin(p * 2.0 * 3.14159265358979323846);
		return pow(20, sinVal);

	}
    

    
};


// =============================================================================
// Child Classes END
// =============================================================================
#endif/* Oscillator_h */
