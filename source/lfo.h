/*
  ==============================================================================

    lfo.h
    Created: 4 May 2021 6:02:40pm
    Author:  刘子源

  ==============================================================================
*/

#pragma once
#include "Oscillators.h"

class lfo
{
public:
    
    void setSampleRate(float sampleRate)
    {
        squareOsc1.setSampleRate(sampleRate);
        sawOsc1.setSampleRate(sampleRate);
        triOsc1.setSampleRate(sampleRate);
        triOsc2.setSampleRate(sampleRate);
        sinOsc1.setSampleRate(sampleRate);
        sinOsc2.setSampleRate(sampleRate);
        
    }
    
    void setFrequency(float frequency)
    {
        squareOsc1.setFrequency(frequency);
        sawOsc2.setFrequency(frequency);
        triOsc1.setFrequency(frequency);
        triOsc2.setFrequency(frequency);
        
        
        
    }
    
    float process()
    {
        sawOsc1.setFrequency(sin(2*3.14*(squareOsc1.process() + 0.5*sawOsc2.process())));
        
        sinOsc1.setFrequency(sawOsc1.process() + (triOsc1.process() + triOsc2.process()));
        
        float output = 0.1*squareOsc1.process() * sinOsc1.process() * 0.5 * (sawOsc1.process() + triOsc2.process());
        
        return output;
        
    }
private:
    SquareOsc squareOsc1;
    SawOsc sawOsc1;
    SawOsc sawOsc2;
    TriOsc triOsc1;
    TriOsc triOsc2;
    SinOsc sinOsc1;
    SinOsc sinOsc2;
    
    
};
