/*
  ==============================================================================

    DelayLine.h
    Created: 1 Mar 2021 3:46:41pm
   

  ==============================================================================
*/

#pragma once
#include <JuceHeader.h>

class DelayLine
{
public:

    ~DelayLine()
    {
        //clean up in destructor
        delete[]  buffer;
    }

    void setSizInSample(int newSizInSamples)
    {   
        size = newSizInSamples;
        buffer = new float[size];
    }

    void setDelayTimeInSample(float delayTimeInSamples)
    {
        readPos = writePos - delayTimeInSamples;
        if (readPos < 0)
        {
            readPos += size;
        }
    }

    float process(float inputSample)
    {
        //float outputVal = buffer[output];
        
        float outputVal = getLineInterpolation();
        //(1) store val with feedback
        buffer[writePos] = inputSample + outputVal * feedback;

        //increment writePos
        writePos ++;
        writePos = writePos % size;


        //(2) recall val from buffer
        //increment readPos
        readPos ++;
        //readPos = readPos % size;
        if (readPos > size)
            readPos -= size;

        return outputVal;
    }

    float getLineInterpolation()
    {
        int indexA = floor(readPos);  //get lower integer
        int indexB = indexA + 1;
        indexB = indexB % size;

        float valA = buffer[indexA];
        float valB = buffer[indexB];

        float frac = readPos - indexA;
        float interpolatiedValue = frac * (valB - valA) + valA;

        return interpolatiedValue;
    }

    void setFeedBack(float _feedback)
    {
        feedback = _feedback;
    }



private:
    int size;
    float readPos;
    int writePos = 0;

    float* buffer;  // pointer to our data

    float feedback = 0;
      
};
