%++++++++++++++++++++++++++++++++++++++
% Tube Screamer Clipping stage -- Symmetric
% Ziyuan Liu
% Mar/29/2021
%++++++++++++++++++++++++++++++++++++++
clear all;
close all;

simulation = 0; % sin wave
% simulation = 1; % impulse 
% simulation = 2; % input wav

if simulation == 0
    
    % Simulation parameters
    SR = 88.2e3;                             % sample rate [Hz]
    dur = 2;                                 % duration of sim [s]
    numSamples = round(SR*dur);              % number of samples in sim



    % Input - create sine wave input
    f0 = 220;                                % frequency [Hz]
    amp = 5;                                 % amplitude of input
    tvec = dur*(0:numSamples-1)'/numSamples; % time vector [s]
    u = amp*sin(2*pi*f0*tvec);               % input vector
    
elseif simulation == 1
    
    % Simulation parameters
    SR = 88.2e3;                             % sample rate [Hz]
    dur = 2;                                 % duration of sim [s]
    numSamples = round(SR*dur);              % number of samples in sim



    % Input - create sine wave input
    f0 = 220;                                % frequency [Hz]
    amp = 5;                                 % amplitude of input
    tvec = dur*(0:numSamples-1)'/numSamples; % time vector [s]
    
    u = zeros(numSamples,1);
    u(1) = 1;
elseif simulation == 2
    amp = 10;
    [u,SR] = audioread('InputSample_s2127317_LIU.wav');
    u = sum(u,2)';
    u = amp*u/max(u);
    numSamples = length(u);
    tvec = (0:numSamples-1)'/numSamples; % time vector [s]
end

% Settings
plotting = true;                         % plotting on/off
audio = true;                            % play output sounds on/off

% Physical parameters
r1 = 10e3;                                 % resistance of R [Ohms]
r2 = 51e3;
r3 = 4.7e3;
r4 = 1e6;
r_dist = 500e3;
c1 = 1e-6;
c2 = 51e-12;
c3 = 47e-9;                           % capacitance of C [F]
Is = 2.52e-9;                            % diode saturation current (A)
Vt = 25.83e-3;                           % diode thermal voltage (V)
Ni = 1.752;                              % diode ideality factor

% calculation parameters
A = -[1/(r1*c1) 0 0; 1/(r3*c2) 1/((r2+r_dist)*c2) 1/(r3*c2); 1/(r3*c3) 0 1/(r3*c3)];
B = [1/(r1*c1); 1/(r3*c2); 1/(r3*c3)];
C = [0; -1/c2; 0];
D = [0 1 0];
E = 0;
F = 0;
L = [-1 1 0];
M = 1;
N = 0;

k = 1/SR;

% derived state-space parameters
Ha = inv(((2/k)*eye(3,3) - A));
Hb = (2/k)*eye(3,3) + A;
K = D * Ha * C + F;
G = 2*Is/(Ni*Vt);

x1 = zeros(3,1);
i1 = 0;
u1 = 0;

to1 = 1e-10;
maxnum = 100;
guesses = zeros(maxnum,1);
residual = guesses;

g = @(v, p) 2*Is*K*sinh(v/(Vt*Ni)) - v + p;         
dg_dv = @(v) ((2*Is*K)/(Ni*Vt))*cosh(v/(Ni*Vt)) - 1;

v = 0;
for n = 1:numSamples
    
% get input
u0 = u(n);

% calculate p[n] from equation (12)
p = D*Ha*(Hb*x1 + B*u1 + C*i1) + (D*Ha*B + E)*u0;


% initialisatio
iter = 0;
delta_v = 1;

% iteration
while (abs(delta_v) > to1) && (iter < maxnum)
    
    % calculate step
    res = g(v,p);
    J = dg_dv(v);
    delta_v = res/J;
    
    % calculate v
    v = v - delta_v;
    
    % store value
    iter = iter + 1;
    guesses(iter) = v;
    residual(iter) = res;
    
end

% linear approx to current through diodes
i0 = (v-p)/K;

% state update
x0 = Ha * (Hb*x1 + B*(u0+u1) + C*(i0 + i1));
% write to output
y(n) = L*x0 + M*u0 + N*i0;

% update variables
i1 = i0;
u1 = u0;
x1 = x0;

end

if plotting == true
    
    if simulation == 0 | simulation == 2
        % figure one
        subplot(2,1,1);
        plot(tvec,u,tvec,y);
        legend('input','output');

        % frequency plot
        Y = fft(y);
        Mag = abs(Y);
        fvec = (0:numSamples-1)*(SR/numSamples);
        Mag_Y = 10 * log10(Mag);
        subplot(2,1,2);
        semilogx(fvec,Mag_Y);
        grid on;
        xlim([0 SR/2]);
        xlabel('frequency');
        ylabel('magnitude');
        title('Spectrum');
    elseif simulation == 1
        
        % figure one
        subplot(2,1,1);
        plot(tvec,u,tvec,y);
        legend('input','output');
        
        % figure 2
        subplot(2,1,2);
        
        Y = fft(y);
        Mag = abs(Y);
        fvec = (0:numSamples-1)*(SR/numSamples);
        Mag_Y = 10*log10(Mag);
        semilogx(fvec,Mag_Y);
        
        
        legend('Spectrum','fc');
        grid on;
        xlabel('frequency');
        ylabel('magnitude');
        title('Spectrum');
    end
        
        
end
